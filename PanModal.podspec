Pod::Spec.new do |s|
  s.name             = 'PanModal'
  s.version          = '1.2.7'
  s.summary          = 'PanModal is an elegant and highly customizable presentation API for constructing bottom sheet modals on iOS.'
  s.description      = 'PanModal is an elegant and highly customizable presentation API for constructing bottom sheet modals on iOS.'
  s.homepage         = 'https://github.com/slackhq/PanModal'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'slack' => 'opensource@slack.com' }
  s.source           = { :git => 'https://bitbucket.org/PremierSoft/panmodal.git', :tag => s.version.to_s }
  s.social_media_url = 'https://twitter.com/slackhq'
  s.ios.deployment_target = '9.0'
  s.swift_version = '5.0'
  s.source_files = 'PanModal/**/*.{swift,h,m}'
end